#include "Customer.h"

Customer::Customer(string name)
{
	this->_name = name;
}

Customer::Customer()
{
}

double Customer::totalSum() const
{
	double sum = 0;
	for (set<Item>::iterator i = _items.begin() ; i != _items.end() ; i++)
	{
		sum += i->totalPrice();
	}
	return sum;
}

void Customer::addItem(Item item)
{
	pair<set<Item>::iterator, bool> p;
	p = _items.insert(item);
	set<Item>::iterator it = p.first;
	if (!p.second)
	{
		Item node = *it;
		_items.erase(it);
		// make changes to the value in place
		node.setCount(node.getCount()+1);
		// reinsert it into the set, but again without needing 
		// to copy or allocate
		_items.insert(node);
	}
}

void Customer::removeItem(Item item)
{
	set<Item>::iterator it = _items.find(item);
	if (it != _items.end())
	{
		if (it->getCount() > 1)
		{
			Item node = *it;
			_items.erase(it);
			// make changes to the value in place
			node.setCount(node.getCount() - 1);
			// reinsert it into the set, but again without needing 
			// to copy or allocate
			_items.insert(node);
		}
		else
		{
			_items.erase(item);
		}
	}
}

set<Item> Customer::getItems()
{
	return _items;
}

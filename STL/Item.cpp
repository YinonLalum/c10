#include "Item.h"

Item::Item(string name, string serial, double price)
{
	_name = name;
	_serialNumber = serial;
	_unitPrice = price;
	_count = 1;
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return _count * _unitPrice;
}

bool Item::operator<(const Item & other) const
{
	return _serialNumber < other._serialNumber;
}

bool Item::operator>(const Item & other) const
{
	return _serialNumber > other._serialNumber;
}

bool Item::operator==(const Item & other) const
{
	return _serialNumber == other._serialNumber;
}

string Item::getName() const
{
	return _name;
}

string Item::getSerial() const
{
	return _serialNumber;
}

double Item::getPrice() const
{
	return _unitPrice;
}

int Item::getCount() const
{
	return _count;
}

void Item::setName(const string name)
{
	_name = name;
}

void Item::setSerial(const string serial)
{
	_serialNumber = serial;
}

void Item::setPrice(const double price)
{
	_unitPrice = price;
}

void Item::setCount(const int count)
{
	_count = count;
}

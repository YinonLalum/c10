#include"Customer.h"
#include<map>
#include <vector>
#include <iostream>

void printMenu();
void modifyItems(Customer& client, Item itemList[], bool toAdd);
bool cmp(const vector<pair<string, double>>::iterator A, const vector<pair<string, double>>::iterator B);

int main()
{
	bool leave = false;
	bool exit = false;
	int ans = 0;
	Customer client;
	string name;
	map<string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};
	
	while (!exit)
	{
		vector<pair<string,double>> prices; //declaring it here to keep it in the scope
		vector<pair<string,double>>::iterator it;
		set<Item> items;
		printMenu();
		cout << "Please enter your choice: ";
		cin >> ans;
		switch (ans)
		{
		case 1:
			cout << "Please enter your name: ";
			cin >> name;

			for (map<string,Customer>::iterator i = abcCustomers.begin(); i != abcCustomers.end(); i++)
			{
				if (i->first == name)
				{
					cout << "There is allready a costumer with this name!!" << endl;
					ans = 0;
				}
			}
			if (!ans)
			{
				break;
			}

			client = Customer(name);
			modifyItems(client, itemList, true);
			abcCustomers[name] = client;
			break;
		case 2:

			cout << "Please enter your name: ";
			cin >> name;

			for (map<string, Customer>::iterator i = abcCustomers.begin(); i != abcCustomers.end(); i++)
			{
				if (i->first == name)
				{
					client = i->second;
					ans = 0;
				}
			}
			if (ans)
			{
				cout << "There is not client with that name!" << endl;
				break;
			}

			while (!leave)
			{
				cout << "1.  Add items" << endl
					<< "2.  Remove items" << endl
					<< "3.  Back to menu" << endl;
				cout << "What would you like to do?" << endl;
				cin >> ans;
				if (ans == 1)
				{
					modifyItems(client, itemList, true);
				}
				else if (ans == 2)
				{
					modifyItems(client, itemList, false);
				}
				else if (ans == 3)
				{
					cout << "Bye" << endl << endl;
					leave = true;
				}
				else
				{
					cout << "INVALID CHOICE" << endl;
				}
			}
			break;
		case 3:
			for (map<string, Customer>::iterator i = abcCustomers.begin(); i != abcCustomers.end(); i++)
			{
				prices.push_back(pair<string,double>(i->first, i->second.totalSum()));
			}
			it = max(prices.begin(), prices.end()-1,cmp);
			cout << "The client who paid the most is: \"" << it->first << "\" with the sum of: " << abcCustomers[it->first].totalSum() << endl;
			cout << "The itmes he bought are: " << endl;
			items = abcCustomers[it->first].getItems();
			for (set<Item>::iterator i = items.begin(); i != items.end(); i++)
			{
				cout << "The item: \"" << i->getName()
					<< "\" for the price of: " << i->getPrice()
					<< " with the serial number of: " << i->getSerial()
					<< endl;
			}
			break;
		case 4:
			cout << "Exiting...." << endl;
			exit = true;
			break;
		default:
			cout << "INVALID CHOICE" << endl;
			break;
		}
	}

	return 0;
}

bool cmp(const vector<pair<string, double>>::iterator A, const vector<pair<string, double>>::iterator B)
{
	return A->second < B->second;
}

void modifyItems(Customer& client, Item itemList[],bool toAdd)
{
	int ans = 0;
	cout << "The items you can buy/remove are(0 to exit): " << endl;

	for (int i = 0; i < 10; i++)
	{
		cout << i + 1 << ". \"" << itemList[i].getName() << "\" for the price of: " << itemList[i].getPrice() << endl;
	}

	do
	{
		cout << "what would you like to modify?" << endl;
		cin >> ans;
		if (!(ans < 0 || ans > 10))
		{
			if (ans == 0)
			{
				break;
			}
			if (toAdd)
			{
				client.addItem(itemList[ans - 1]);
			}
			else
			{
				client.removeItem(itemList[ans - 1]);
			}
		}
		else
		{
			cout << "INVALID CHOICE" << endl;
		}
	} while (ans != 0);
}

void printMenu()
{
	cout << "Welcome to MagshiMart!" << endl
		 << "1.      to sign up as customer and buy items" << endl
		 << "2.      to uptade existing customer's items" << endl
		 << "3.      to print the customer who pays the most" << endl
		 << "4.      to exit" << endl << endl;
		
}